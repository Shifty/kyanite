# Kyanite Collector

[![apex](https://i.imgur.com/1GVFeT6.png)](https://luciascipher.com/)

## What Is Kyanite

[Kyanite](https://en.wikipedia.org/wiki/Kyanite) is a typically blue silicate mineral,
commonly found in aluminium-rich metamorphic pegmatites and/or sedimentary rock.
Kyanite in metamorphic rocks generally indicates pressures higher than four kilobars.
Although potentially stable at lower pressure and low temperature,
the activity of water is usually high enough under such conditions that it is
replaced by hydrous aluminosilicates such as muscovite, pyrophyllite, or kaolinite.
Kyanite is also known as disthene, rhaeticite and cyanite.

## How to install and run

### Via Cargo

```sh
cargo install kyanite
# Make sure your $CARGO_HOME/bin is in your $PATH
```

> Try `kyanite --help` for more command line options.
